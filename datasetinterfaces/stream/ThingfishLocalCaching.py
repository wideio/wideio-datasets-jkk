import time
import random
import numpy as np
 
def numpy_random(n):
    """Return a list of n random floats in the range [0, 1)."""
    return np.random.random((n)).tolist()
 
def numpy_randint(a, b, n):
    """Return a list of n random ints in the range [a, b]."""
    return np.random.randint(a, b, n).tolist()

class CachedItem(object):
    def __init__(self, key, value, duration=10):
        self.key = key
        self.value = value
        self.duration = duration
        self.timeStamp = time.time()

    def __repr__(self):
        return '<CachedItem {%s:%s} expires at: %s>' % (self.key, self.value, time.time() + self.duration)

class CachedDict(dict):

    # be nice to make this an iterator ...
    # multithreading may make it awkward, though ...
    
    # also, should remember to base64 encode this stuff
    # on the way in, decode on the way out ...

    def get(self, key, fn, duration):
        if key not in self \
            or self[key].timeStamp + self[key].duration < time.time():
                print 'adding new value'
                o = fn(key)
                self[key] = CachedItem(key, o, duration)
        else:
            print 'loading from cache'

        return self[key].value

if __name__ == "__main__":

    sz=200
    cd=CachedDict()
    
    fn = lambda key: 'key is %s' % key

    for i, v in enumerate(numpy_randint(0, 49, sz)):
        ci = CachedItem(i, v)

    for i in range(1,sz):
        print cd.get(i, fn, 5)
    

