import numpy as np
from LoadStreamingPickle import FunnelStream

def numpy_random(n):
    """Return a list of n random floats in the range [0, 1)."""
    return np.random.random((n)).tolist()
 
def numpy_randint(a, b, n):
    """Return a list of n random ints in the range [a, b]."""
    return np.random.randint(a, b, n).tolist()

if __name__ == "__main__":
    
    s=FunnelStream(tempfile.TemporaryFile())

    max=1000
    sz=max*max
    n=0

    while n<max:
        l=numpy_random(sz)
        s.dump(l)
        
        
