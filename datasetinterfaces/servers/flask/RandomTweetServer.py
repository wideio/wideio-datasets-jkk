""" 
TweetServer.py: serve n tweets from the processed (no emoticons) file
as a batch via a flask web server. We usually have n \in [200, 500] if
we are displaying on a web page, but the limits are usually imposed by
server and client limitations RAM and network latency.
"""

from StreamCorpus import TwitterCorpusStream, Indexable
from flask import Flask

from flask import Flask
app = Flask(__name__)

# initialise our twitstream object
twit=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/training.1600000.processed.noemoticon.csv')

it=Indexable(twit)

@app.route("/data/sentiment140/random/<int:n>")
def getTwitFromTwotStream(n):
    # N.B. zero error checking here on whether or not
    # this is a proper twut ...
    #d, m = twit.GetElement(n)
    d, m = it[n]
    out=m.id + " : " + d
    return out

if __name__ == "__main__":
    #app.run(debug=True)
    # & when we're ready for prime time:
    app.run(host='0.0.0.0', debug=True)
