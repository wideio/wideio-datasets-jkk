class DatasetInterface:

    def GetAvailableMetadata(self):
        pass
        
    def GetMetadata(self,metadata):
        pass

    def GetElement(self, id):
        pass

    def UpdateParameters(self, 
                         source, 
                         verbose=False):
        pass

    def Keys(self):
        pass

    def GetFullname(self):
        pass

    def GetDescription(self):
        pass
