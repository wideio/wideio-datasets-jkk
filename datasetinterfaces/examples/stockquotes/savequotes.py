from GetStockQuotes import get_stock_quote
from threading import Timer
#from time import sleep, time
import datetime, time
from datasetinterfaces.stream.cache.memcachedict import MemCacheDict
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# right. we can do some monkeying about with the key.

# 1) we should be able to add stuff to the key, like a date, and
# send *that* through to the memcached. that way the stock
# prices keep on increasing in size.

# 2) furthermore, we monkey about with the key on the client end,
# so that we just put in a stock name and get the latest one back
# - i.e., one that doesn't have the date thing attached. *this is
# why we need the local copy!!!* that & iteration ...

# 3) we can do *history* - we can ask for the previous data point, and
# the one before that - dunno how yet, but we'll work it out ... yeah,
# we monkey with the key in the client - BP.L:2 means t-2, BP.L:5 means
# t-5, and so on ...


def get_quotes(symbols):
    l=[]
    for s in symbols:
        l.append(get_stock_quote(s))
    return l

def get_quote_symbols(file):
    with open(file, 'r') as f:
        alist = [line.strip() for line in f]
    return alist



next_call=time.time()

mdict=MemCacheDict(host='localhost', 
                   port=11211)

def foo(symbols):
    global next_call
    global mdict
    now=datetime.datetime.now()
    print 'Executing at: ', now
    next_call=next_call+300
    # now here we can get all the stock symbols ...
    for s in symbols:
        l=get_stock_quote(s)
        # can't have "control chars" (i.e. space) in the key, so
        # we get rid of 'em ...
        _s=s+':'+str(datetime.datetime.now()).replace(' ', '_')
        print 'Info for ', _s, ' : ', l
        # ... & then we store them
        logging.info("foo(): adding items to MemCacheDict")
        mdict[str(_s)]=l
        logging.info("foo(): adding items to MemCacheDict")
    

    # we might need a dict class where the keys have been 
    # mucked about with ...

    # notice the slightly manky notation used to pass a
    # param to the Timer object ...

    Timer( next_call - time.time(), foo, [symbols] ).start()

if __name__=="__main__":
    stock_symbols='FTSE100.txt'
    l=get_quote_symbols(stock_symbols)
    foo(l)
