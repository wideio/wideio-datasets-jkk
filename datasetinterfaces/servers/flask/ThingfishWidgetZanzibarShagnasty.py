import os, gensim, sys, csv, linecache
import nltk
import itertools
from itertools import dropwhile
from random import randrange

# need to be able to do this kind of thing ...
#top5 = itertools.islice(array, 5)
# list(itertools.islice(it, 0, n, 1))

# provided so that we can have statements of the type:

# it=Indexable(TwitterCorpusStream())
# print(it[10])
# print(it[2:12:2])
# ...

# LATEST:

# /home/jkk/Documents/progz/python/wide.io-platform-datamunging/flask

# mv to git repo when ready ...

class Indexable(object):
    def __init__(self,it):
        self.it = iter(it)
    def __iter__(self):
        for elt in self.it:
            yield elt
    def __getitem__(self,index):
        try:
            return next(itertools.islice(self.it,
                                         index,
                                         index+1))
        except TypeError:
            return list(itertools.islice(self.it,
                                         index.start,
                                         index.stop,
                                         index.step))

# we'll have gensim for demonstration purposes, for now

def iter_documents(top_directory):
    """
    Generator: iterate over all relevant documents, yielding one
    document (=list of utf8 tokens) at a time.
    """
    # find all .txt documents, no matter how deep under top_directory
    for root, dirs, files in os.walk(top_directory):
        for fname in filter(lambda fname: fname.endswith('.txt'), files):
            # read each document as one big string
            document = open(os.path.join(root, fname)).read()
            # break document into utf8 tokens
            yield gensim.utils.tokenize(document, lower=True, errors='ignore')

class TxtSubdirsCorpus(object):
    """
    Iterable: on each iteration, return bag-of-words vectors,
    one vector for each document.

    Process one document at a time using generators, never
    load the entire corpus into RAM.

    """
    def __init__(self, top_dir):
        self.top_dir = top_dir
        # create dictionary = mapping for documents => sparse vectors
        self.dictionary = gensim.corpora.Dictionary(iter_documents(top_dir))

    def __iter__(self):
        """
        Again, __iter__ is a generator => TxtSubdirsCorpus is a streamed iterable.
        """
        for tokens in iter_documents(self.top_dir):
            # transform tokens (strings) into a sparse vector, one at a time
            yield self.dictionary.doc2bow(tokens)

class CorpusStream():
    """
    An iterable class: for each iteration, return a raw representation
    of the text in the corpus. The root directory of the corpus should
    be passed as a parameter when the object is being constructed.

    One document at a time is processed using generators, thus saving on
    the amount of RAM required to process the corpus.
    """

    def __init__(self,
                 root_dir,
                 ext='.*\.txt'):
        
        self.root_dir = root_dir
        # create our corpus reader, which we'll use to stream
        # text back to our calling program
        self.reader = nltk.corpus.PlaintextCorpusReader(self.root_dir,
                                                             ext)
        self.length=len(self.reader.fileids())

    def Keys(self):
        """
        Keys() method: returns the file IDs from the corpus reader.
        """
        return self.reader.fileids()

    def size(self):
        """
        size() method: return the size of the corpus in terms of how
        many files there it consists of.
        """
        return self.length

    def _metadata(self, doc):
        """
        _metadata() method: provide basic metadata on this text.
        In cases where the text comes from an NLTK corpus, this
        is simple because the corpora have a standard structure.

        Subclasses may have to override this method for nonstandard
        corpora.
        """

        [category, filename] = doc.split('/')

        class TextMetadata:
            def __init__(self, category, filename):
                self.category = category
                self.filename = filename

        return TextMetadata(category, filename)

    def __iter__(self):
        """
        __iter__() method: each iteration yields the text
        of a document from the corpus. __iter__ follows the
        generator pattern to preserve memory.
        """

        for doc in self.reader.fileids():
            yield self.reader.raw(doc), self._metadata(doc)

    def GetElement(self, id):
        """
        GetElement(id) method: if given a correct ID (one returned from
        the Keys() method, for example) will return the contents of the
        text as a string.
        """
        return self.reader.sents(id)


    # these guys need to be implemented in subclasses

    def GetAvailableMetadata(self):
        """ 
        GetAvailableMetadata() method: unimplemented in CorpusStream;
        should be implemented in classes which inherit from it.
        """
        pass


    def GetFullname(self):
        """ 
        GetFullName() method: unimplemented in CorpusStream;
        should be implemented in classes which inherit from it.
        """
        pass

    def GetDescription(self):
        """ 
        GetDescription() method: unimplemented in CorpusStream;
        should be implemented in classes which inherit from it.
        """
        pass


# JKK: 2DO: need something that'll give us the number of lines
# in the file (length method)

class TwitterCorpusStream(CorpusStream):
    """
    An iterable class specialized to stream tweets from the
    sentiment140 tweet collection.

    The class is a generator, so iterating through the collection
    will return one tweet at a time.
    """

    # may as well do this here, since we'll be using this on a
    # repeated basis, rather than one bulk read ...

    def __init__(self,
                 src):
        """
        ___init__(self, src) method:
            @param self: we ought to know what this is by now
            @param src : the name of the file containing the tweets
        """
        self.src = src
        self.keys = []
        self.count = 0
        for line in open(src).xreadlines(  ): 
            self.keys.append(self.count)
            self.count += 1
        self.FIELDNAMES = ('polarity', 'id', 'date', 'query', 'author', 'text')
        self.reader = None #csv.DictReader(f, fieldna)
        self.it = Indexable(self)

    def iterate_from_line(self, f, start_from_line):
        return (l for i, l in dropwhile(lambda x: x[0] < start_from_line, enumerate(f)))

    def __iter__(self):
        """
        ___iter__() method: Iterate over the tweet collection one at a time.
        return: A stirng containing the text of the tweet
        """

        with open(self.src, 'r') as f:
            self.reader  = csv.DictReader(f,
                                    fieldnames=self.FIELDNAMES,
                                    delimiter=',',
                                    quotechar='"')
            for i,d in enumerate(self.reader):
                # & here we wrap this to get certain fields
                yield self._metadata(d)

    def _metadata(self, doc):
        """
        _metadata() method: provide metadata on the tweet from
        the sentiment140 corpus. The metadata is composed of:

            'id':       unique ID of the tweet
            'date':     date on which the tweet was sent (UTC)
            'author':   author of the tweet
            'polarity': the class to which the tweet belongs:
                0-4, negative to positive

        """

        class TextMetadata:
            def __init__(self,
                         id,
                         date,
                         author,
                         polarity):
                self.id = id
                self.date = date
                self.author = author
                self.polarity = polarity
        
        # collect the keys we've got so far here 

        return(doc['text'], TextMetadata(doc['id'],
                            doc['date'],
                            doc['author'],
                            doc['polarity']))


    def GetAvailableMetadata(self):
        """
        GetAvailableMetadata() method: return the field names used in 
        the sentiment140 tweet collection.
        """
        return self.FIELDNAMES

    def GetFullname(self):
        """
        GetFullname() method: returns a description of the corpus.
        """
        return "TwitterSentiment140"

    def GetDescription(self):
        description="""
        Dataset taken from the sentiment140 corpus. The tweets are classified
        as strongly negative to strongly positive, on a scale of 0 to 4
        """
        return description

    def size(self):
        return self.count

    def Keys(self):
        """
        Keys() method: return a list of the IDs of indvidual tweets
        """
        return self.keys

    def GetElement(self, id):
        """
        GetElement(id) method: returns the specified line
        from the sentiment140 dataset. The line will take the form 
        of the text of the tweet and the metadata; both will be
        returned to the calling program.
        @param id: integer argument, tweet to be returned.
        """

        with open(self.src, 'r') as f:
            self.reader  = csv.DictReader(f,
                                          fieldnames=self.FIELDNAMES,
                                          delimiter=',',
                                          quotechar='"')
            for line_number, line in enumerate(self.reader):
                if line_number == id:
                    return self._metadata(line)

if __name__ == "__main__":

    # test code, give the above classes a bit of a workout
    # need to make sure this thing points to the right directories on
    # my laptop/this thing ...

    # JKK: many of these tests have been run but they were edited
    # out since they take a *long* time to run ...

#    root_dir='/home/jkk/nltk_data/corpora/movie_reviews'

#    t=CorpusStream(root_dir)

#    for doc, metadata in t:
#        print doc
#        print "Category: ", metadata.category
#        print "Filename: ", metadata.filename

#    twit=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/testdata.manual.2009.06.14.csv')

    twit=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/training.1600000.processed.noemoticon.csv')

    print "getting TwitterCorpusStrem.Keys()"

    keys=twit.Keys()
    
    print "Testing random access methods for twot stream"

    # get the size of the corpus ...
    size=twit.size()

    print "Size of twitterbase: ", twit.size()

    # ... and  let's go ahead & get some random tweets from it

    l=[randrange(size) for i in range(100)]

    import time
    start = time.clock()

    for i in l:
        print "getting item: ", i
        d, m = twit.GetElement(i)
        #print i, " : ", m.id, " : ", d
        print d

    end = time.clock()

    print "Time taken: ", end - start

    # all the code below works & tests out fine, but it
    # goes on a bit ...
    
    sys.exit(0)


#    twit=TwitterCorpusStream('/home/johnny/Documents/progz/python/scikit/parallel_ml_tutorial-master/datasets/sentiment140/testdata.manual.2009.06.14.csv')
    
    for twot, meta in twit:
        print twot
        print "id:       ", meta.id
        print "date:     ", meta.date
        print "author:   ", meta.author
        print "polarity: ", meta.polarity

    print "Size of file in lines/tweets: ", twit.size()

    twit1=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/training.1600000.processed.noemoticon.csv')
 
#   twit1=TwitterCorpusStream('/home/johnny/Documents/progz/python/scikit/parallel_ml_tutorial-master/datasets/sentiment140/training.1600000.processed.noemoticon.csv')
   
    for twot, meta in twit1:
        print twot
        print "id:       ", meta.id
        print "date:     ", meta.date
        print "author:   ", meta.author
        print "polarity: ", meta.polarity

    root_dir='/home/jkk/Documents/nimbus/test/emails/classification/data/test'

    print " on Enron email corpus"

    t=CorpusStream(root_dir)

    for doc, metadata in t:
        print doc
        print "Category: ", metadata.category
        print "Filename: ", metadata.filename
    

    
