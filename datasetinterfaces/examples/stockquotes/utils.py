def get_quote_symbols(file):
    with open(file, 'r') as f:
        alist = [line.strip() for line in f]
    return alist

