from flask import Flask
app = Flask(__name__)

def isprime(n):
    for x in range(2,n):
        if n%x == 0:
            return False
    return True

@app.route("/data/<int:n>")
def isThisAPrime(n):
    if isprime(n):
        return "YAY!!! It's a prime!!!"
    else:
        return "Composite number ... Life sucks sometimes ..."


if __name__ == "__main__":
    app.run(debug=True)
    # & when we're ready for prime time:
    # app.run(host='0.0.0.0')
