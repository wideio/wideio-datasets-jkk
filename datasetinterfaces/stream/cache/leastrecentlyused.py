import logging
import itertools
import collections
import threading
import operator
from datetime import datetime, timedelta
from hiddenvaluedict import HiddenValDict
from threading import Timer

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

###
# JKK: need to be aware of __del__() method here if we include the
# backing store in this class - TBD. 
###

###
# JKK: also need to look out for the policy thing here - need
# JKK: to have a Policy object. 
###

# this is going to be an arbitrary number for now, we'll worry about
# passing it as a param or whatever later, when we have the threading
# working ...

class LeastRecentlyUsedDict(HiddenValDict):

    # might need to pass ttl in as a param ...
    def __init__(self, max=1000, kill=100, policy='LRU', *args, **kwargs):
        # call the parent to get e.g store attribute instantiated ...
        logging.info("%s: ___init__()", self.__class__.__name__) 
        super(LeastRecentlyUsedDict, self).__init__(*args, **kwargs)
        self.policy=policy
        self.is_running = False
        self.max=max
        self.kill=kill

    def _run(self):
        logging.info("%s: ___init__()", self.__class__.__name__) 
        self.is_running = False
        self.start()
        self.cull()

    def start(self):
        logging.info("%s: start()" % self.__class__.__name__) 
        #  self.__class__.__name__
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        logging.info("%s: stop()" % self.__class__.__name__) 
        self._timer.cancel()
        self.is_running = False

    # one way of doing this would be to reset the ttl field
    # in the HiddenValDict - we're assuming we have data like 
    # this:

    # {1 : ['a', 9],
    #  2 : ['b', 17], ..., }

    # and we just want to reset the last data item to

    def __getitem__(self, key):
        logging.info("%s: __getitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        # *assumes* this exists - well, if they're using this
        # class, they should know *roughly* what they're
        # getting ...
        self.store[key][1]+=1
        return self.__valuetransform__(self.store[key])

    def cull(self):
        logging.info("%s: cull()" % self.__class__.__name__) 
        if len(self.store) >= self.max:
            self.store=sorted(store.items(), key=lambda e: e[1][1])
            for i, k in enumerate(self.store):
                # delete some of these guys ...
                if i < self.kill:
                    del(self.store[k])
                if i == self.kill:
                    break
            
if __name__ == "__main__":

    now=datetime.now()

    d=dict({1 : ['a', 0],
            2 : ['b', 0],
            3 : ['c', 0],
            4 : ['d', 0],
            5 : ['e', 0],})

    logging.info("main(): printing values to be input")
    for k, v in d.items():
        print k, ' => ', v

    md=LeastRecentlyUsedDict()

    logging.info("main(): adding items to LeastRecentlyUsedDict()")
    for k, v in d.items():
        md[k]=v

    logging.info("main(): getting items from LeastRecentlyUsedDict()")
    for k, v in md.items():
        print k, ' => ', v

    logging.info("main(): __dump__ from LeastRecentlyUsedDict()")
    _d=md.__dump__()

    for k, v in _d:
        print k, ' => ', v

#from time import sleep
#
#def hello(name):
#    print "Hello %s!" % name
#
#print "starting..."
#rt = RepeatedTimer(1, hello, "World") # it auto-starts, no need of rt.start()
#try:
#    sleep(5) # your long-running job goes here...
#finally:
#    rt.stop() # better in a try/finally block to make sure the program ends!
