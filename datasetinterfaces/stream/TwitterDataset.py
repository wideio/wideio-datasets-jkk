# quick test code on how to do an iterable class

# so to do this we need:

# Iterator objects in python conform to the iterator protocol, which basically means they provide two methods: __iter__() and next(). The __iter__ returns the iterator object and is implicitly called at the start of loops. The next() method returns the next value and is implicitly called at each loop increment. next() raises a StopIteration exception when there are no more value to return, which is implicitly captured by looping constructs to stop iterating.

# the metadata is the full list of fields in a dict

# we build a list with all the data in, but the user
# can iterate through the tweets + polarity, or the
# metadata itself

from Dataset import DatasetInterface

csv_file='testdata.manual.2009.06.14.csv'
FIELDNAMES = ('polarity', 'id', 'date', 'query', 'author', 'text')

# from itertools import irange

class TwitterSentimentDataset(DatasetInterface):

    def __init__(self):
        self.data = []
        self._read_csv()

    def _read_csv(self, 
                 csv_file=csv_file, 
                 fieldnames=FIELDNAMES, 
                 max_count=None):

        import csv  # put the import inside for use in IPython.parallel

        self.csv_file = csv_file

        with open(csv_file, 'rb') as f:
            reader = csv.DictReader(f, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')
            for i, d in enumerate(reader):
                if d['polarity'] == '4' or d['polarity'] == 0:
                    self.data.append(d)
                    
    def GetAvailableMetadata(self):
        return FIELDNAMES

    def GetMetadata(self,metadata):
        class SubDataset:
            def Keys(self2): 
               return self.Keys()
            def GetElement(self2,id):
               return self.data[id][metadata]
        return SubDataset()

    def GetElement(self, id):
        # need to check whether this is in range
        # else throw a wobbler ...
        d = self.data[id]
        return d['text']

    def UpdateParameters(self, 
                         csv_file=csv_file, 
                         verbose=False):
        self.csv_file = csv_file
        if verbose:
            print 'Updating parameters: rereading data ...'
        self._read_csv()
        if verbose:
            print 'Updating parameters completed ...'

    def Keys(self):
        return range(len(self.data))

    def GetFullname(self):
        return "TwitterSentiment140"

    def GetDescription(self):
        description="""
Dataset taken from the sentiment140 corpus. Only those tweets
labelled positive or negative are returned"""
        return description


if __name__ == '__main__':

    tc=TwitterSentimentDataset()

    for k in tc.Keys():
        t = tc.GetElement(k)
        print t

    tc.UpdateParameters(verbose=True)

    print "Description: ", tc.GetFullname()

    md = tc.GetAvailableMetadata()

    for m in md:
        print "Metadata: ", m

    p=tc.GetMetadata('polarity')
    
    print "Metadata (polarity): ", p

    p = tc.GetMetadata('text')

    for i in range(0,100):
        tmp = p.GetElement(i)
        print i, " : ", tmp

    p = tc.GetMetadata('date')

    for i in range(0,100):
        tmp = p.GetElement(i)
        print i, " : ", tmp

