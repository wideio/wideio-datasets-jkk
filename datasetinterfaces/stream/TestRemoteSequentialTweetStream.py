import matplotlib.pyplot as pl

import numpy as np
import pickle
import time
import urllib2
import re

# need WHERE clause ...
# (filter on keys)
# ... also a directory!


class TweetStreamBroker:
    """ Gets tweets from a webserver. """
    def __init__(self, url, block=500):
        """
        TweetStreamBroker ctor: instantiates the tweet stream broker
        object.
        @param url: url of the data source, e.g. http://localhost:5000/data/sentiment140/seq/0
        """
        self.url=url
        self.idx=0
        self.block=block
        self.twits=[]
        self.response=''
        print 'self.url: ', self.url
    
    def __iter__(self):
        self.twits=[]
        request=self.url + '/' + str(self.idx)
        print '__iter()__: request: ', request
        for tweet in urllib2.urlopen(request):
            #re.sub("</br>", "\n", tweet)
            self.twits = tweet[0].split('</br>')
            print '__iter()__: ', tweet
            self.twits.append(tweet)
        self.idx = self.idx + self.block
        yield self.twits

    
if __name__ == "__main__":
    
    url='http://127.0.0.1:5000/data/sentiment140/seq'
    t=TweetStreamBroker(url)
    
    idx=0

    for twits in t:
        print 'type(twits): ', type(twits)
        # print 'twits: ', twits
        for tweet in twits:
            dat=re.split(r'</br>', tweet)
            for d in dat:
                print "index: ", idx, " tweet: ", dat
                idx += 1
