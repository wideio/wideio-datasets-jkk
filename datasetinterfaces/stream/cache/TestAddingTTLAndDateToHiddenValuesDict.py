import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import itertools
import collections
from datetime import datetime, timedelta
from hiddenvaluedict import HiddenValDict

# have to make this a unit test down the line ...

if __name__=="__main__":

    ttl=200
    
    now=datetime.now()

    d=dict({1 : ['a', now, ttl],
            2 : ['b', now, ttl],
            3 : ['c', now, ttl],
            4 : ['d', now, ttl],
            5 : ['e', now, ttl],})

    logging.info("main(): printing values to be input")
    for k, v in d.items():
        print k, ' => ', v

    md=HiddenValDict()

    logging.info("main(): adding items to HiddenValDict()")
    for k, v in d.items():
        md[k]=v

    logging.info("main(): getting items from HiddenValDict()")
    for k, v in md.items():
        print k, ' => ', v

    logging.info("main(): __dump__ from HiddenValDict()")
    _d=md.__dump__()

    for k, v in _d:
        print k, ' => ', v




    
    
