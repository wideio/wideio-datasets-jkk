## @package EnronDatasetSentimentCategories

# EnronDatasetSentimentCategories module: this module contains a class
# intended to facilitate access to the Enron email corpus. This version
# of the corpus has been catgorised according to sentiment / emotional
# content for approximately 1700 emails.

# See http://bailando.sims.berkeley.edu/enron_email.html for further
# details.

import nltk, re
from nltk.corpus import PlaintextCorpusReader
from Dataset import DatasetInterface

corpus_root='/home/jkk/Documents/progz/python/sentiment-analysis/data/original_enron_with_categories'
categories_doc=corpus_root + '/' + 'categories.txt'

class EnronSentimentCategoriesDataset(DatasetInterface):
    ## The EnronSentimentCategoriesDataset class provides access
    #  to classified emails from the Enron email corpus. There are
    #  a number of different categories (see GetAvailableMetadata())
    #  into which the emails have been placed."""

    def __init__(self, corpus_root=corpus_root):
        ## __init__ method
        #  @param corpus_root: where to find the corpus
        #  (default value used if not value passed)
        #  __init__ creates two NLTK corpus readers
        #  one looks for txt (email) files
        #  the other looks for category files for each email
        self.corpus_root = corpus_root
        self.text_reader = PlaintextCorpusReader(self.corpus_root,
                                                 r'.*\.txt')
        self.metadata_reader = PlaintextCorpusReader(self.corpus_root, 
                                                     r'.*\.cats')
        with open(categories_doc, 'r') as f:
            self.categories = f.read()

    def Keys(self):
        ## Keys() method:
        #  returns the (email) file IDs for calling code to
        #  iterate over 
        return self.text_reader.fileids()

    def GetDescription(self):
        ## GetDescription() method:
        #  returns a short description of the Enron corpus
        description="""
The Enron email database, classified into different sentiment categories.
See GetAvailableMetadata() for description of the categories. 
"""
    def GetAvailableMetadata(self):
        ## GetAvailableMetadata() method:
        #  returns the contents of the categories file
        #  provided with the corpus
        return self.categories

    def GetMetadata(self, id):
        ## GetMetadata(self, id) method:
        #  @param id: name of the file containing
        #  the email
        #  returns: the contents of id.cats file
        import string
        nu_id = string.replace(id, '.txt', '.cats')
        return self.metadata_reader.raw(nu_id)

    def GetElement(self, id):
        ## GetElement(self, id) method:
        #  @param id: name of the file containing 
        #  the email
        #  returns: the contents of the email
        return self.text_reader.raw(id)
        
    def GetFullName(self):
        ## GetFullName() method:
        #  returns a description of the data set
        return "Enron email database subset classified by sentiment"

    def UpdateParameters(self, 
                         corpus_root=corpus_root,
                         verbose=False):
        ## UpdateParameters(self, 
        #                   corpus_root=corpus_root,
        #                    verbose=False) method:
        #  this method takes a new corpus root and reads
        #  in the data
        #  verbose=True will report what it's up to 
        
        self.corpus_root = corpus_root
        if verbose:
            print 'Updating parameters: rereading data ...'
        self.text_reader = PlaintextCorpusReader(self.corpus_root,
                                                 r'.*\.txt')
        self.metadata_reader = PlaintextCorpusReader(self.corpus_root, 
                                                     r'.*\.cats')
        if verbose:
            print 'Updating parameters completed ...'

if __name__ == '__main__':

    print 'Testing GetAvailableMetadata()'
    cd = EnronSentimentCategoriesDataset()

    print cd.GetAvailableMetadata()

    keys = cd.Keys()

    print 'number of keys: ', len(keys)

    for k in cd.Keys():
        print k

    for k in cd.Keys()[:20]:
        print "Getting metadata for ", k
        print cd.GetMetadata(k)

    for k in cd.Keys()[:20]:
        print "Getting email's first lines for ", k
        print cd.GetElement(k)





