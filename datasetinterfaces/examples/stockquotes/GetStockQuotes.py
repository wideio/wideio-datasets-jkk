import ystockquote
import json
import pprint
import urllib2

# google version
def get_google_quote(symbol):   
    url = 'http://finance.google.com/finance/info?q=%s' % symbol
    lines = urllib2.urlopen(url).read().splitlines()
    return json.loads(''.join([x for x in lines if x not in ('// [', ']')]))

# yahoo version
def get_yahoo_quote(symbol):
    print 'Recvd the following:', symbol
    return ystockquote.get_all(symbol)

def get_stock_quote(symbol, broker='yahoo'):
    # just the two choices implemented at the moment
    if broker == 'yahoo':
        return get_yahoo_quote(symbol)
    else:
        return get_google_quote(symbol)


    
