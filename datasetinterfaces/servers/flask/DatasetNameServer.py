"""
DatasetNameServer: serve the names and URLs of named datasets hosted
at WIDE.IO on a know port

"""

from flask import Flask
from random import randrange
app = Flask(__name__)

# hack which will be replaced by a database at some point

sources={ 'sentiment140Batch' : 'localhost:5000/data/sentiment140/seq',
          'sentiment140BatchWithMetadata' : 'http://localhost:5000/data/sentiment140WithMetadata/seq',
}

@app.route("/data/nameddatasets/")
def showNamedDatasets():

    print 'type(sources) : ', type(sources)

    out=''
    for k in sources:
        out+=k + ' : ' + sources[k] + '</br>'

    return out

if __name__ == "__main__":
    #app.run(debug=True)
    # & when we're ready for prime time:
    app.run(host='0.0.0.0', port=5001, debug=True)

        





