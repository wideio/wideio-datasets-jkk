""" 
TweetServer.py: serve n tweets from the processed (no emoticons) file
as a batch via a flask web server. We usually have n \in [200, 500] if
we are displaying on a web page, but the limits are usually imposed by
server and client limitations RAM and network latency.
"""

from sentimentanalysis.datastreaming.StreamCorpus import TwitterCorpusStream, Indexable
from flask import Flask
import itertools

from flask import Flask
app = Flask(__name__)

# initialise our twitstream object
twit=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/training.1600000.processed.noemoticon.csv')

block=500

@app.route("/data/sentiment140/seq/<int:n>")
def getTwitsFromTwotStream(n):

    # N.B. zero error checking here on whether or not
    # this is a proper twut ...

    # fingers crossed this bloody works ...

    it=Indexable(twit)
    _l=it[n:n+block]
    l=[]
    
    for i,j in _l:
        #str=j.id + " : " + i + '\n'
        l.append(i)

    out='</br>'.join(l)
    
    return out

@app.route("/data/sentiment140WithMetadata/seq/<int:n>")
def getTwitsAndMetadataFromTwotStream(n):

    # N.B. zero error checking here on whether or not
    # this is a proper twut ...

    # fingers crossed this bloody works ...

    it=Indexable(twit)
    _l=it[n:n+block]
    l=[]
    
    for i,j in _l:
        str=j.id + " : " + i + '\n'
        l.append(str)

    out='</br>'.join(l)
    
    return out


if __name__ == "__main__":
    #app.run(debug=True)
    # & when we're ready for prime time:
    app.run(host='0.0.0.0', debug=True)
