from StreamCorpus import TwitterCorpusStream, Indexable
from random import randrange
import itertools
from itertools import dropwhile
import time

def iterate_from_line(f, start_from_line):
    return (l for i, l in dropwhile(lambda x: x[0] < start_from_line, enumerate(f)))

for line in iterate_from_line(open(filename, "r", 0), 141978):
    DoSomethingWithThisLine(line)


def doStuff(twit, n):
    it=Indexable(twit)
    t, m = it[i]
    return t, m

if __name__ == "__main__":
    
    # initialise our twitstream object
    twit=TwitterCorpusStream('/home/jkk/Documents/progz/python/sentiment-analysis/data/sentiment140/training.1600000.processed.noemoticon.csv')

    size=twit.size()
    
    print "Size of twitterbase: ", twit.size()
    
    # ... and  let's go ahead & get some random tweets from it

    l=[randrange(size) for i in range(100)]

    #it=Indexable(twit)

    start = time.time()

    

#    for i in l:
#        t, m = list(itertools.islice(twit, i, i+1))[0]
#        print i, " : ", m.id, " : ", t

    end = time.time()

    print "Time taken: ", end - start
