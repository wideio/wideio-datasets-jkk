import logging
import itertools
import collections

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# inspired by posts from:
# http://stackoverflow.com/questions/2390827/how-to-properly-subclass-dict-and-override-get-set
# http://stackoverflow.com/questions/3387691/python-how-to-perfectly-override-a-dict?rq=1

# we're subclassing from collections.MutableMapping due to advice from the
# 2nd SO answer

class HiddenValDict(collections.MutableMapping):

###
# You can write an object that behaves like a dict quite easily with 
# ABCs (Abstract Base Classes) from the collections module.
# It even tells you if you missed a method, so below is the minimal 
# version that shuts the ABC up.    
###

    def __init__(self, *args, **kwargs):
        self.store = dict()
        logging.info("%s: ___init__()", self.__class__.__name__) 
        # yeah, but, hmm, don't we need to do stuff with the
        # input data here?
        self.update(dict(*args, **kwargs))  # use the free update to set keys
        logging.info("HiddenValDict: ___init__(): done self.update()") 

    def __getitem__(self, key):
        logging.info("%s: __getitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        return self.__valuetransform__(self.store[key])

    def __setitem__(self, key, value):
        logging.info("%s: __setitem__ %s['%s'] = %s" % (self.__class__.__name__,
                                                        str(self),
                                                        str(key),
                                                        str(value)))
        self.store[key] = value

    def __delitem__(self, key):
        logging.info("%s: __delitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        del self.store[self.key]

    def __iter__(self):
        logging.info("%s: __iter__(): ('%s')" % (self.__class__.__name__,
                                                 str(self)))
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    # JKK: this is (one) of the things we need to hack ...

    def __valuetransform__(self, values):
        logging.info("%s: __valuetransform__:  type(values) %s" \
                     % (self.__class__.__name__, str(type(values))))
        return values[0]

    def __dump__(self):
        logging.info("%s: __dump__(): ('%s')" % (self.__class__.__name__,
                                                 str(self)))
        # return dict(self.store)
        for k in self.store:
            yield k, self.store[k]

# here's some of the stuff we can do with this ...

# this is the e.g. where they transform the keys into
# lower case

#s = MyTransformedDict([('Test', 'test')])

#assert s.get('TEST') is s['test']   # free get
#assert 'TeSt' in s                  # free __contains__
                                    # free setdefault, __eq__, and so on

#import pickle
#assert pickle.loads(pickle.dumps(s)) == s
                                    # works too since we just use a normal dict

if __name__ == "__main__":

    d=dict({1 : ['a', 'f1.txt'],
            2 : ['b', 'f2.txt'],
            3 : ['c', 'f3.txt'],
            4 : ['d', 'f4.txt'],
            5 : ['e', 'f5.txt'],})
            

    for k, v in d.items():
        print k, ' => ', v
    
    md=HiddenValDict()

    logging.info("main(): adding items to HiddenValDict()")
    for k, v in d.items():
        md[k]=v

    logging.info("main(): getting items from HiddenValDict()")
    for k, v in md.items():
        print k, ' => ', v

    logging.info("main(): __dump__ from HiddenValDict()")
    _d=md.__dump__()

    for k, v in _d:
        print k, ' => ', v
