from Dataset import DatasetInterface
import urllib2
import json

class RemoteDataset(DatasetInterface):

    """ 
    RemoteDataset class: This class is intended to provide access
    to a WIDE.IO hosted dataset, implementing the various methods in
    the parent DatasetInterface. The intention is for the class to be
    given the name of the dataset, and to be aware of all the various
    access methods (e.g. URL etc.) by which access to the dataset can
    be enabled.
    """

    def __init__(self, dataset, serialisation=json):
        """
        ___init__(dataset, serialisation): create a RemoteDataset
        object.
        @param dataset: URL of the dataset.
        @param serialisation: format of the data, set by defaut to JSON. 
        """
        
        self.dataset=datset
        self.serialisation=serialisation

    def GetAvailableMetadata(self):
        pass
        
    def GetMetadata(self,metadata):
        pass

    def GetElement(self, id):
        # JKK: this is what we have to aim for: 
        # return self.serialisation.loads(urllib.urlopen(url+"/getelement/"base64.encode(self.serialisation(id)))+"/" ))
        pass

    def UpdateParameters(self, 
                         source, 
                         verbose=False):
        pass

    def Keys(self):
        pass

    def GetFullname(self):
        pass

    def GetDescription(self):
        pass

