from GetStockQuotes import get_stock_quote
from threading import Timer
import datetime, time, memcache, random, logging
from datasetinterfaces.examples.stockquotes.stockquotecachesource import StockQuoteCacheSource

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# we *could* add an mdict ... to handle queries if
# the server falls over, for e.g., so we can carry
# on processing & serving quotes ...

# think about this idea; only worthwhile if there's a lot
# of traffic on the memcache server

def get_quote_symbols(file):
    with open(file, 'r') as f:
        alist = [line.strip() for line in f]
    return alist

def messWithTheCache(s, symbols):
    stock=random.choice(symbols)
    choice=random.uniform(0,1)
    logging.info("%s: get(): choice is %f" % \
                 (__name__, choice))
    if choice < 0.5:
        # get the most-recent cache data point

        logging.info("%s: get(): getting current quote for %s" % \
                     (__name__, stock))
        res=s.getQuote(stock)
    else:
        extension=random.randrange(0,20)
        logging.info("%s: get(): we want time offset %d" % \
                     (__name__, extension))
        search=stock+':'+str(extension)
        logging.info("%s: get(): getting historical quote for %s" % \
                     (__name__, search))
        res=s.getQuote(search)

    print res

if __name__=="__main__":
    
    stock_symbols='FTSE100.txt'
    symbols=get_quote_symbols(stock_symbols)
    s=StockQuoteCacheSource()
    # get a quote, using the offset notation, & one without
    for stock in ['WPP.L:4', 'BP.L']:
        res=s.getQuote(stock)
        logging.info("%s: get(): getting quote for %s" % \
                     (__name__, stock))
        print res

    for stock in symbols:
        print "Getting data for stock"
        res=s.getQuote(stock)
        logging.info("%s: get(): getting quote for %s" % \
                     (__name__, stock))
        print res

    
