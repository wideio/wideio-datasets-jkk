from GetStockQuotes import get_stock_quote
from threading import Timer
import datetime, time, memcache, random, logging
from sys import stdout
from datasetinterfaces.examples.stockquotes.utils import get_quote_symbols
from datasetinterfaces.stream.Dataset import DatasetInterface

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', 
                    level=logging.INFO) #, 
                    # this is a terrific idea
                    # really neat. choose where you're going to send
                    # your error messages to
                    # really, a great, simple idea
                    # pity it doesn't bloody work ...
                    #stream=stdout)



# we *could* add an mdict ... to handle queries if
# the server falls over, for e.g., so we can carry
# on processing & serving quotes ...

# think about this idea; only worthwhile if there's a lot
# of traffic on the memcache server

desc="""
The FTSE 100 data set is a listing of the top 100 equities listed on the
London Stock Exchange. It's an example of an open data set, where data is
constantly being updated.
"""


# might need to abstract some of this away to
# a parent class ...

fields={'stock_exchange': "string", 
        'fifty_two_week_low': "float", 
        'two_hundred_day_moving_avg' : "float",
        'market_cap': "float",
        'fifty_two_week_high': "float",
        'price_sales_ratio': "float",
        'price_earnings_growth_ratio': "float",
        'price': "float",
        'fifty_day_moving_avg': "float",
        'price_book_ratio': "float",
        'earnings_per_share': "float",
        'volume': "float",
        'book_value': "float",
        'short_ratio': "float",
        'price_earnings_ratio': "float",
        'dividend_yield': "float",
        'avg_daily_volume': "float",
        'ebitda': "string (e.g. 377.5M)",
        'change': "float",
        'dividend_per_share': "float"
}


class StockQuoteCacheSource:
    
    """
    StockQuoteCacheSource: provides a stream of stock market quotes.
    It also provides historical quote information. Data points are
    returned in the form of a dictionary. 
    """
    def __init__(self, 
                 host='localhost', 
                 port=11211, 
                 name='FTSE 100',
                 desc=desc):
        # could abstract these guys - they could go into a parent
        # class ...
        self.host=host
        self.port=port
        self.name=name
        self.desc=desc
        logging.debug("%s: __init__(): @params: host: %s, port: %d" % \
                     (self.__class__.__name__, self.host, self.port))
        self.client=memcache.Client([(self.host, self.port)])
        logging.debug("%s: __init__(): getting self.t" % \
                     self.__class__.__name__)
        self.t=self.client.get('t')
        if self.t is None:
            error="Unable to initialise, problem with memcached on %s:%s?" % \
                (self.host, self.port)
            raise Exception(error)
        logging.debug("%s: __init__(): self.t == %s" % \
                     (self.__class__.__name__, self.t))

    def GetFullname(self):
        """ 
        GetFullname(): return the name of the (underlying) dataset
        """
        return self.name

    def getCurrentIteration(self):
        """
        getCurrentIteration: fetch the current timestep value t
        from memcached. 
        return: current value of t
        """
        # could also abstract these ...
        # JKK: need a way of finding whether or not this
        # JKK: is returning properly, & if not raise an
        # JKK: exception!
        logging.debug("%s: getCurrentIteration(): getting t" % \
                     self.__class__.__name__)
        return self.client.get('t')


    def _get(self, symbol):
        """
        _get: gets the symbol from memcached, if available
        @param symbol: name of an equity as a string, e.g. 
        BP.L, WPP.L
        return: the most recent value of t as returned from
        memcached.
        """
        logging.debug("%s: get(): getting quote for %s" % \
                      (self.__class__.__name__, symbol))
        return self.client.get(symbol)

    def get(self, symbol, _fields=None):
        
        logging.debug("%s: get(): getting quote for %s:%s" % \
                      (self.__class__.__name__, symbol, _fields))
        _dict=self._get(symbol)
        if _dict:
            for key, value in _dict.iteritems():
                print 'get: %s => %s : %s' % (symbol, key, value)
            if _fields:
                return dict([(i, _dict[i]) for i in _fields if i in _dict])
        return _dict

    def rewindUntilLocate(self, symbol, t, _fields=None):
        """
        rewindUntilLocate: search for a symbol with a timestep
        t in the cache. Recurses backwards, decrementing the
        timestep with each invocation, until it reaches 0. If
        no data point is found, return None.
        
        @param symbol: name of the equity
        @param t     : time step, from current t back to 0
        return       : values of the datapoint from the cache,
                       or None if not found. 
        """
        if t < 0:
            logging.debug("%s: rewindUntilLocate(): t == %d, bailing!" % \
                         (self.__class__.__name__, t))
            return None
        search=symbol+':'+str(t)
        logging.debug("%s: rewindUntilLocate(): getting quote for %s" % \
                     (self.__class__.__name__, search))
        result=self.get(search, _fields)
        if result is not None:
            logging.debug("%s: rewindUntilLocate(): GOT quote for %s" % \
                         (self.__class__.__name__, search))
            return result
        else:
            logging.debug("%s: rewindUntilLocate(): FAILED quote for %s" % \
                         (self.__class__.__name__, search))
            logging.debug("%s: rewindUntilLocate(): t == %d, recursing ..." % \
                         (self.__class__.__name__, t))
            return self.rewindUntilLocate(symbol, t-1), _fields

    # it's tempting to turn this into an interator, maybe
    # a dict - but let's leave it as a method for now ...

    def getQuote(self, stock, _fields=None):
        logging.debug("%s: getQuote(): parameter is: %s" % \
                     (self.__class__.__name__, stock))
        l=stock.split(':')
        if len(l) == 1:
            # 'ordinary' (nonhistorical) price requested, so
            # we want the current (most recent) value of the stock
            self.t=self.getCurrentIteration()
            # don't know if we need the dict, let's make use of
            # the memcache client ...
            logging.debug("%s: getQuote(): getting quote for %s" % \
                         (self.__class__.__name__, stock))
            return self.rewindUntilLocate(stock, int(self.t), _fields)
        elif len(l) == 2:
            logging.debug("%s: getQuote(): getting quote for %s" % \
                         (self.__class__.__name__, stock))
            # this may not work instantly, since we may not have the
            # current increment loaded in memcache (yet) ...
            result=self.get(stock)
            if result is not None:
                return result
            else:
                # need to split the stock into it's constituent
                # parts here, just in case we have to recurse a few 
                # time steps to find this data point ...
                # i know i have a list with these above, but this
                # is less foul code ... (sort of) ...
                stock=stock.split(':')[0]
                t=int(self.t)-1
                return self.rewindUntilLocate(stock, t, _fields)
        else:
            # raise an exception here, don't recognise
            # the format of the stock
            raise Exception("Problem with quote ", stock, ": not available?")

# test function called from main below

# might be worth moving some or all of the functions from above
# to this class ...

class StockQuoteDataset(DatasetInterface):
    """
    StockQuoteDataset: implementation of the DatasetInterface class to
    provide access to stock quote data. 
    """
    
    def __init__(self, s, symbols):
        """
        __init__(s): Create an instance of StockQuoteDataset
        @param s      : StockQuoteCacheSource instance
        @param symbols: list of the symbols on the exchange
        """
        self.s=s
        self.symbols=symbols

    def Keys(self):
        """
        Keys(): provide  all the keys of the stock market dataset.
        """
        # or we make it a global variable in a module, & allow
        # a number of different classes to access it ...
        
        # t=int(self.s.getCurrentIteration())
        
        # we have to iterate through this so that we 
        # ensure all the entries are there ...

        # otherwise we could just get the current t value
        # & decrement it until it's 0 & return that

        # but we're not guaranteed that all the entries
        # will be there; so we have to check 'em ...

        return self.symbols

    def GetElement(self, id):
        """
        GetElement(): get an element from the data set.
        @param id   : identifier of the element.
        return      : element from the data set.
        """

        return self.s.getQuote(id)

    def UpdateParameters(self, source, verbose=False):
        # unimplemented; as yet unsure of what this could
        # mean in this context.
        pass

    def GetAvailableMetadata(self):
        """
        GetAvailableMetadata(): return available metadata.
        """
        # GetAvailableMetadata() deosn't really make sense in this
        # context, so we'll just return:
        return "No available metadata"

    def GetMetadata(self):
        """
        GetMetadata(): return the fields from the stock market data
        provider.
        """
        return fields
    
    def GetFullname(self):
        return self.s.GetFullname()

    def GetDescription(self):
        return self.s.desc

def messWithTheCache(s, symbols):
    stock=random.choice(symbols)
    choice=random.uniform(0,1)
    logging.debug("%s: get(): choice is %f" % \
                 (__name__, choice))
    if choice < 0.5:
        # get the most-recent cache data point

        logging.debug("%s: get(): getting current quote for %s" % \
                     (__name__, stock))
        res=s.getQuote(stock)
    else:
        extension=random.randrange(0,20)
        logging.debug("%s: get(): we want time offset %d" % \
                     (__name__, extension))
        search=stock+':'+str(extension)
        logging.debug("%s: get(): getting historical quote for %s" % \
                     (__name__, search))
        res=s.getQuote(search)

    print res

if __name__=="__main__":
    
    stock_symbols='FTSE100.txt'
    symbols=get_quote_symbols(stock_symbols)
    s=StockQuoteCacheSource()
    # get a quote, using the offset notation, & one without
    for stock in ['WPP.L:4', 'BP.L']:
        res=s.getQuote(stock)
        logging.debug("%s: get(): getting quote for %s" % \
                     (__name__, stock))
        print res

    for i in range(50):
        messWithTheCache(s, symbols)

    other_s=StockQuoteDataset(s, symbols)
    
    other_s.UpdateParameters('bouncy')

    thing=other_s.GetAvailableMetadata()
    print "Available metadata: ", thing
    
    thing=other_s.GetMetadata()
    print "Metadata: ", thing

    thing=other_s.GetFullname()
    print "Full name: ", thing

    thing=other_s.GetDescription()
    print "Description: ", thing

    print 'Keys of data set:'

    for k in other_s.Keys():
        print k

    for stock in ['WPP.L:4', 'BP.L']:
        res=other_s.GetElement(stock)
        logging.debug("%s: get(): getting quote for %s" % \
                     (__name__, stock))
        print res

    _fields=[ 'two_hundred_day_moving_avg', 
              'fifty_two_week_low',
              'fifty_two_week_high']

    for stock in symbols:
        res=s.getQuote(stock, _fields)
        logging.info("%s: get(): getting quote for %s" % \
                     (__name__, stock))
        print res
    


    
    
