# Iterator objects in python conform to the iterator protocol, which basically means they provide two methods: __iter__() and next(). The __iter__ returns the iterator object and is implicitly called at the start of loops. The next() method returns the next value and is implicitly called at each loop increment. next() raises a StopIteration exception when there are no more value to return, which is implicitly captured by looping constructs to stop iterating.

# question here is: what is the metadata?

import nltk
import re
from nltk.corpus import PlaintextCorpusReader
from Dataset import DatasetInterface

# default, so have to change this when it's in production
corpus_root = '/home/jkk/Documents/nimbus/test/emails/classification/corpus'

class EnronHamSpamDataset(DatasetInterface):
                    
    def GetAvailableMetadata(self):
        pass

    def GetMetadata(self,id):
        words = len(self.reader.words(id))
        sents = len(self.reader.sents(id))
        s = ''
        if re.match('ham', id):
            s = 'ham'
        elif re.match('spam', id):
            s = 'spam'
        else:
            s = 'unclassified'
        class EnronHamSpamSubDataset:
            def __init__(self, id, words, sents, s):
                self.id = id
                self.words = words
                self.sents = sents
                self.s = s
                self._str = 'ID: ' + self.id
                self._str += ' Num. words: ' +  str(self.words)
                self._str += ' Num. sentences: ' + str(self.sents)
                self._str += ' Sentiment: ' + self.s
                
            def __repr__(self):
                return "Class EnronHamSpamDataset: " + self._str
                
            def __str__(self):
                return self._str

        return EnronHamSpamSubDataset(id, words, sents, s)

    def GetElement(self, id):
        return self.reader.sents(id)

    def UpdateParameters(self, 
                         corpus_root=corpus_root,
                         verbose=False):
        self.corpus_root=corpus_root
        if verbose:
            print 'Updating parameters: rereading data ...'
        self.reader = PlaintextCorpusReader(self.corpus_root, '.*')
        if verbose:
            print 'Updating parameters completed ...'

    def Keys(self):
        return self.reader.fileids()

    def GetFullname(self):
        return "Enron email database, classified as ham/spam"

    def GetDescription(self):
        description="""
The Enron email database, classified into ham and spam.
See www.cs.cmu.edu/~./enron for more details.
"""
        return description

    def __init__(self, corpus_root=corpus_root):
        self.corpus_root = corpus_root
        print 'Reading corpus'
        self.reader = PlaintextCorpusReader(self.corpus_root, '.*')
        print 'Read corpus: __init__ DONE'

if __name__ == '__main__':

    tc=EnronHamSpamDataset()
    
    print 'Description: ', tc.GetDescription()

    keys=tc.Keys()
    
    for k in keys[:20]:
        l = tc.GetElement(k)
        for s in l:
            print s

    print 'Testing GetMetadata()'

    for k in keys[:20]:
        s = tc.GetMetadata(k)
        print s

    print 'Tested GetMetadata()'

#    print 'Checking update parameters'

# this works fine, rather long winded though!

#    tc.UpdateParameters(corpus_root='/home/jkk/nltk_data/corpora/enron')

#    for k in keys[:20]:
#        l = tc.GetElement(k)
#        for s in l:
#            print s

#    print 'Checked update parameters'

    


#        t = tc.GetElement(k)
#        print t
#
#    tc.UpdateParameters(verbose=True)
#
#    print "Description: ", tc.GetFullname()
#
#    md = tc.GetAvailableMetadata()
#
#    for m in md:
#        print "Metadata: ", m
#
#    p=tc.GetMetadata('polarity')
#    
#    print "Metadata (polarity): ", p
#
#    p = tc.GetMetadata('text')
#
#    for i in range(0,100):
#        tmp = p.GetElement(i)
#        print i, " : ", tmp
#
#    p = tc.GetMetadata('date')
#
#    for i in range(0,100):
#        tmp = p.GetElement(i)
#        print i, " : ", tmp
#
