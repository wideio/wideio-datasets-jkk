import logging
import itertools
import collections
import memcache

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# question here is, do we inherit from LRU dict?

# also, two ways of doing this: we can have *(everything* go to the memcache
# server, or we can store locally as well (which will make the response
# faster ...)

class MemCacheDict(collections.MutableMapping):

    def __init__(self, host='localhost', port='666'):
        self.store = dict()
        self.host=host
        self.port=port
        logging.info("%s: ___init__()", self.__class__.__name__) 
        logging.info("%s: ___init__(): creating memcache client: \
                      %s:%s", self.__class__.__name__, host, port)
                     
        self.client=memcache.Client([(host, port)])

        if len(self.client.get_stats()) == 0:
            raise Exception("Server ", host, " not available")
            
        #self.update(dict(*args, **kwargs))  
        
    def __getitem__(self, key):
        logging.info("%s: __getitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        return self.store[key]

    def __setitem__(self, key, value):
        logging.info("%s: __setitem__ %s['%s'] = %s" % (self.__class__.__name__,
                                                        str(self),
                                                        str(key),
                                                        str(value)))
        # JKK: store in memcache ...
        val=self.client.set(str(key), value)
        if val != 1:
            logging.info("%s: __setitem__ ['%s']: \
                          problem with host %s" % (self.__class__.__name__,
                                                        str(key),
                                                        str(self.host)))

            raise Exception("Server ", host, " not available")
        self.store[str(key)] = value

    def __delitem__(self, key):
        logging.info("%s: __delitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        # JKK: memcache equiv ...

        val=self.client.delete(key)
        if val != 0:
            logging.info("%s: __setitem__ ['%s']: \
                          problem with host %s" % (self.__class__.__name__,
                                                        str(key),
                                                        str(self.host)))
            raise Exception("Server ", host, " not available")

        del self.store[key]

    def __iter__(self):
        logging.info("%s: __iter__(): ('%s')" % (self.__class__.__name__,
                                                 str(self)))
        # JKK: there IS no memcache equiv to this ...
        # JKK: so we provide it with our local store ...
        return iter(self.store)

    def __len__(self):
        return len(self.store)

if __name__ == "__main__":

    # okay, that's working, now we need to get this working with
    # e.g. a stock market feed (stream of values)

    # the idea here is we have a client which attaches to the
    # server spitting out stock market values - it gets the most
    # recent ...

    # then we make a request to the server & sleep for a bit ...

    d=dict({1 : ['a', 'f1.txt'],
            2 : ['b', 'f2.txt'],
            3 : ['c', 'f3.txt'],
            4 : ['d', 'f4.txt'],
            5 : ['e', 'f5.txt'],})
            

    for k, v in d.items():
        print k, ' => ', v
    
    md=MemCacheDict(host='127.0.0.1', port=11211)

    logging.info("main(): adding items to MemCacheDict()")
    for k, v in d.items():
        md[k]=v

    logging.info("main(): getting items from MemCacheDict()")
    for k, v in md.items():
        print k, ' => ', v

