"""
RandomStockQuoteServer: serve stock quotes using the caching mechanism. We
use the cache as a way of a) providing historical data & b) cutting down on
network latency.
"""

import memcache
from datasetinterfaces.examples.stockquotes.stockquotecachesource import StockQuoteCacheSource
import logging
import sys
from datasetinterfaces.utils import log_wrap

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
#logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

src = StockQuoteCacheSource()
from flask import Flask
app = Flask(__name__,template_folder='./')

# these guys are always going to be random, i suppose ...
# so we should have another path here ...

@app.route("/data/FTSE/random/<string:symbol>")
def getQuoteFromStockStream(symbol):
    this='getQuoteFromStockStream'
    from flask import render_template
    logging.info("%s: getting quote for %s" % \
                 (this,symbol))
    res=src.getQuote(str(symbol))
    if res is None:
        logging.info("%s: FAILED getting quote for %s" % \
                     (this,symbol))
        return render_template('error.html', symbol=symbol)
    else:
        output=log_wrap(res)
        logging.info("%s: getQuote returned %s" % \
                     (this,output))
        logging.info("%s: got quote for %s" % \
                     (this,symbol))
        return render_template('thingfish.html', res=res)

# this is the type of data we're getting back:

# {'stock_exchange': '"London"', 'fifty_two_week_low': '1006.0001', 'two_hundred_day_moving_avg': '1186.99', 'market_cap': '41.332B', 'fifty_two_week_high': '1355.50', 'price_sales_ratio': '215.65', 'price_earnings_growth_ratio': '1.99', 'price': '1213.50', 'fifty_day_moving_avg': '1198.08', 'price_book_ratio': '123.42', 'earnings_per_share': '0.685', 'volume': '775102', 'book_value': '9.881', 'short_ratio': 'N/A', 'price_earnings_ratio': '1780.29', 'dividend_yield': '1.48', 'avg_daily_volume': '5048412', 'ebitda': '10.608B', 'change': '-6.00', 'dividend_per_share': '18.02'}

# need to make it look pretty ... ?!?

if __name__=="__main__":
    app.run(host='0.0.0.0', debug=True)
