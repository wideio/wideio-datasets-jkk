from setuptools import setup

setup(name='datasetinterfaces',
      version='0.2',
      description='Provide access to data sets',
      #url='https://bitbucket.org/wideio/sentiment-analysis',
      author='Johnny Kelsey',
      author_email='jkk@wide.io',
      license='MIT',
      packages=['datasetinterfaces',
                'datasetinterfaces.stream',
                'datasetinterfaces.stream.cache',
                'datasetinterfaces.servers',
                'datasetinterfaces.servers.flask',
                'datasetinterfaces.examples',
                'datasetinterfaces.examples.stockquotes',],
      zip_safe=False)
