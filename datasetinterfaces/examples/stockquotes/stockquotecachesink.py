from GetStockQuotes import get_stock_quote
from threading import Timer
#from time import sleep, time
import datetime, time, memcache, logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# right. we can do some monkeying about with the key.

# 1) we should be able to add stuff to the key, like a date, and
# send *that* through to the memcached. that way the stock
# prices keep on increasing in size.

# 2) furthermore, we monkey about with the key on the client end,
# so that we just put in a stock name and get the latest one back
# - i.e., one that doesn't have the date thing attached. *this is
# why we need the local copy!!!* that & iteration ...

# 3) we can do *history* - we can ask for the previous data point, and
# the one before that - dunno how yet, but we'll work it out ... yeah,
# we monkey with the key in the client - BP.L:2 means t-2, BP.L:5 means
# t-5, and so on ...

def get_quotes(symbols):
    l=[]
    for s in symbols:
        l.append(get_stock_quote(s))
    return l

def get_quote_symbols(file):
    with open(file, 'r') as f:
        alist = [line.strip() for line in f]
    return alist


# client.set("counter", "10")
# client.incr("counter")
# print "Counter was incremented on the server by 1, now it's %s" %
# client.get("counter")

class StockQuoteCacheSink:

    def __init__(self, host='localhost', port=11211, offset=180):
        self.host=host
        self.port=port
        self.offset=offset
        self.next_call=time.time()
        self.client=memcache.Client([(self.host, self.port)])
        self.t=self.client.get('t')
        if self.t is None:
            # it *may* be the case that the memcache server has
            # been flushed, in which case we set t here ...
            logging.info("__init__(): setting t")
            self.client.set('t', '0')
            self.t=self.client.get('t')

        logging.info("__init__(): t set to %s" % self.t)

    def incrementIteration(self):
        logging.info("incrementIteration(): incrementing t")
        self.client.incr('t')
            
    def getCurrentIteration(self):
        logging.info("getCurrentIteration(): getting t")
        return self.client.get('t')

    def iterate(self, symbols):
        logging.info("iterate(): current iteration: %s" % self.t)
        self.incrementIteration()
        self.t=self.getCurrentIteration()
        logging.info("iterate(): updated iteration: %s" % self.t)
        now=datetime.datetime.now()
        self.next_call=self.next_call+self.offset
        # now here we can get all the stock symbols ...
        for s in symbols:
            l=get_stock_quote(s)
            _s=s+':'+str(self.t)
            logging.info("iterate(): for stock %s, storing as: %s" % (s,_s))
            self.client.set(_s, l)            
            print "Data for ", _s, " is: ", l
        Timer( self.next_call - time.time(), self.iterate, [symbols] ).start()

if __name__=="__main__":
    stock_symbols='FTSE100.txt'
    l=get_quote_symbols(stock_symbols)
    s=StockQuoteCacheSink(offset=300).iterate(l)

# okay, so we have a way of getting the text in there ...

# what am i i trying to do? surely what i'm trying to do is
# save the data points
# load the most recent data points
# provide a method to download previous (historical) data points

# we have a server. we can increment a number each time the server
# stores keys. this can also be queried:



# def getCurrentIteration(self):
#     return self.t




# we just keep incrementing this, and use it to store the next
# data point

# md['AAL.L:325'] = l

# & we can interrogate the cache for e.g. md['AAL.L:324'], md['AAL.L:323'],
# etc. jolly good.

# so, how do i do this? also, does the code get locked, so i can't
# do anything else with it?

# question ... how do we get the latest key from memcache?
# answer   ... piece of cake, use the incr command. easy!




# why this is important:

# we can have one server just permanently updating the database
# we can have another handling the queries

# divide & conquer! it's nifty, since we get the data from the
# same source - just incrementing the cache!

