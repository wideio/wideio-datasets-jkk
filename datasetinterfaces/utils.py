def log_wrap(mydict):
    """ 
    Takes dictionary and returns a string containing key-value pairs 
    separated by a carraige return.
    @param mydict: input dictinary to be formatted as a string
    returns: formatted output string
    """
    mystr = '\n'.join(['%s = %s' % (k,v) for k,v in mydict.iteritems()])
    return mystr

