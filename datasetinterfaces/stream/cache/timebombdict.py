import logging
import itertools
import collections
import threading
from datetime import datetime, timedelta
from hiddenvaluedict import HiddenValDict
from threading import Timer

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

###
# JKK: need to be aware of __del__() method here if we include the
# backing store in this class - TBD. 
###

###
# JKK: also need to look out for the policy thing here - need
# JKK: to have a Policy object. 
###

# this is going to be an arbitrary number for now, we'll worry about
# passing it as a param or whatever later, when we have the threading
# working ...

class TimeBombDict(HiddenValDict):

    # might need to pass ttl in as a param ...
    def __init__(self, ttl=300, policy='LRU', *args, **kwargs):
        # call the parent to get e.g store attribute instantiated ...
        logging.info("%s: ___init__()", self.__class__.__name__) 
        super(TimeBombDict, self).__init__(*args, **kwargs)
        logging.info("TimeBombDict: ___init__()") 
        self.ttl=ttl
        self.policy=policy
        self.is_running = False
        self.now=datetime.now()

    def _run(self):
        self.is_running = False
        self.start()
        self.cull()

    def start(self):
        #  self.__class__.__name__
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False

    # one way of doing this would be to reset the ttl field
    # in the HiddenValDict - we're assuming we have data like 
    # this:

    # {1 : ['a', now, ttl],
    #  2 : ['b', now, ttl], ..., }

    # and we just want to reset the last data item to
    # self.ttl. 

    def __getitem__(self, key):
        logging.info("%s: __getitem__ %s['%s']" % (self.__class__.__name__,
                                                   str(self),
                                                   str(key)))
        # *assumes* this exists - well, if they're using this
        # class, they should know *roughly* what they're
        # getting ...
        self.store[key][2]=self.ttl
        return self.__valuetransform__(self.store[key])
    


if __name__ == "__main__":

    ttl=200
    
    now=datetime.now()

    d=dict({1 : ['a', now, ttl],
            2 : ['b', now, ttl],
            3 : ['c', now, ttl],
            4 : ['d', now, ttl],
            5 : ['e', now, ttl],})

    logging.info("main(): printing values to be input")
    for k, v in d.items():
        print k, ' => ', v

    md=TimeBombDict()

    logging.info("main(): adding items to TimeBombDict()")
    for k, v in d.items():
        md[k]=v

    logging.info("main(): getting items from TimeBombDict()")
    for k, v in md.items():
        print k, ' => ', v

    logging.info("main(): __dump__ from TimeBombDict()")
    _d=md.__dump__()

    for k, v in _d:
        print k, ' => ', v
    


    
    
