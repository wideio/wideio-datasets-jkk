import urllib2
from Dataset import DatasetInterface

class NamedRemoteDataset(DatasetInterface):

    """ 
    NamedRemoteDataset class: This class is intended to provide access
    to a WIDE.IO hosted dataset, implementing the various methods in
    the parent DatasetInterface. The intention is for the class to be
    given the name of the dataset, and to be aware of all the various
    access methods (e.g. URL etc.) by which access to the dataset can
    be enabled.
    """
    def __init__(self, dataset):
        """
        __init__(dataset): constructs a NamedRemoteDataset object
        @param dataset: name of the remote dataset
        """
        self.sources=self._getSources()
        self.dataset=dataset

        # need to be able to clear off & check whether or not the 
        # dataset exists, if not we sulk ...

        if not self._found():
            raise ValueError("Value dataset not found, maybe a type?")

    def GetDescription(self):
        str="""NamedRemoteDataset class: This class is intended to provide access
to a WIDE.IO hosted dataset, implementing the various methods in
the parent DatasetInterface."""
        return str

        
    def Keys(self):
        """
        Keys() method: return the names of all the remote named datasets.
        """
        return self.sources.keys()

    def GetAvailableMetadata(self):
        """
        GetAvailableMetadata(self): don't really have metadata apart from the URLs 
        of the named remote dataset, so we'll just go ahead and return them in a list.
        """
        return self.sources.values() 


    def GetMetadata(self):
        """
        GetMetadata() method: return the URLs of all the named remote datasets.
        """
        return self.sources.items()

    def GetElement(self, id):
        """
        GetElement method: return the URL for the named remote dataset.
        @param id: name of the remote dataset.
        return: URL of the dataset
        """
        return self.sources[id]

    def UpdateParameters(self, source, verbose=False):
        raise NotImplementedError("Not applicable to class RemoteDataset")

    def _found(self):
        """
        _found: check for the existence of a named remote dataset in our collection
        of datasets.
        return: True if found, else False.
        """
        if self.dataset in self.sources.keys():
            return True

        return False
        
    def _getSources(self):
        """
        _getSources(): this method looks up the data source named
        in self.dataset, first by looking it up by making a request
        to a well-known port service at WIDE.IO (actually, at the 
        moment, localhost ...), and then by looking it up in a local
        file which is part of the distribution of WIDE.IO.
        """
        
        url='http://localhost:5001/data/nameddatasets/'

        # debug, just return this when you're happy with it ...

        return dict([ i.split(" : ") for i in [r for r in urllib2.urlopen(url)][0].split("</br>")[:-1]])

        # this is a fallback, but we won't ever use it ...
        # this is a named remote dataset; if they can't find out
        # where the dataset is by querying the web server, then
        # no point in carrying on this far, is there?
#        file="sources.csv"
#        with open(file, 'r') as f:
#            reader=DictReader(f, 
#                                  fieldnames=['Name', 'Source'],
#                                  delimiter=',', quotechar='"')
#            for i, d in enumerate(reader):
#                tmp[d['Name']] = d['Source']
#                
#        return tmp

if __name__ == "__main__":

    # list of datasets - two should be fine, one should
    # throw an exception ...
    dataset_list=['sentiment140BatchWithMetadata',
                  'MrsAndMissusPointlessEnquiry',
                  'sentiment140Batch',
                  ]

    for ds in dataset_list:
        print 'Trying to construct a named remote dataset called %s' % ds
        try:
            n=NamedRemoteDataset(ds)    
        except ValueError:
            print 'An exception happened! For dataset %s. Oh, noes!' % ds
        else:
            print 'Constructed a named remote dataset %s' % ds


    for k in n.Keys():
        print 'Got key: ', k

    for k in n.GetMetadata():
        print 'Got URL: ', k
    
    for ds in dataset_list:
        print 'Trying to retrieve URL of named remote dataset called %s' % ds
        try:
            n.GetElement(ds)
        except KeyError:
            print 'Dataset %s not found!' % ds
        else:
            print 'Retrieved URL of named remote dataset called %s' % ds

    try:
        n.UpdateParameters('SasquatchBotherer')
    except NotImplementedError:
        print 'UpdateParameters not yet implemented'

    print n.GetDescription()

    l=n.GetAvailableMetadata()

    for i, r in enumerate(l):
        print 'Metadata: %d => %s ' % (i,r) 
